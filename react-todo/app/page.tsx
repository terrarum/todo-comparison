import TodoList from '@/app/components/todo-list';
import styles from './page.module.css'

export default function Home() {
    return (
        <div className={styles.container}>
            <h1>React Todo</h1>
            <TodoList/>
        </div>
    )
}
