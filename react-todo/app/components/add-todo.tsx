import {useState} from 'react';

export default function AddTodo(props) {
    const [title, setTitle] = useState('');

    function inputChangeHandler(event) {
        setTitle(event.target.value);
    }

    function addTodo() {
        props.addTodo(title);
        setTitle('');
    }

    function inputKeyDownHandler(event) {
        if (event.key === 'Enter') {
            addTodo();
        }
    }

    return (
        <div>
            Add Item: <input type="text" value={title} onChange={inputChangeHandler} onKeyDown={inputKeyDownHandler}/>
            <button onClick={addTodo}>Add</button>
        </div>
    )
}
