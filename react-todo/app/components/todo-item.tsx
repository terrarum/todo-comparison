import Todo from '../../../models/Todo.js';

export default function TodoItem(props) {
    const todo: Todo = props.todo;

    return (
        <li>
            <input type="checkbox" defaultChecked={todo.completed} onChange={() => props.toggleCompleted(todo)}/>
            {todo.title}
        </li>
    )
}
