'use client';
import Todo from '../../../models/Todo';
import TodoItem from '@/app/components/todo-item';
import AddTodo from '@/app/components/add-todo';
import {useState} from 'react';

export default function TodoList() {
    const [todos, setTodos] = useState((): Todo[] => [
        {title: 'Buy milk', completed: false},
        {title: 'Buy eggs', completed: true},
    ]);

    function toggleCompleted(todo: Todo) {
        setTodos(todos.map(t => {
            if (t.title === todo.title) {
                t.completed = !t.completed;
            }
            return t;
        }));
    }

    function addTodo(title: string) {
        if (!title) return;
        setTodos([...todos, {title, completed: false}]);
    }

    return (
        <div>
            <AddTodo addTodo={addTodo}/>
            <ul>
                {todos.map(todo => <TodoItem key={todo.title} todo={todo} toggleCompleted={toggleCompleted}/>)}
            </ul>
            Completed {todos.filter(todo => todo.completed).length} of {todos.length} tasks.
        </div>
    )
}
